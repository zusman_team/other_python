from __future__ import  annotations
from abc import ABC, abstractmethod


############ Factories ############

class AbstractFactory(ABC):
    @abstractmethod
    def create_product_a(self):
        pass

    @abstractmethod
    def create_product_b(self):
        pass


class ConcreteFactory1(AbstractFactory):
    def create_product_a(self):
        return ConcreteproductA1()

    def create_product_b(self):
        return ConcreteproductB1()


class ConcreteFactory2(AbstractFactory):
    def create_product_a(self):
        return ConcreteproductA2()

    def create_product_b(self):
        return ConcreteproductB2()

########### Products #############

class AbstractProductA(ABC):
    @abstractmethod
    def useful_function_a(self):
        pass

class AbstractProductB(ABC):
    @abstractmethod
    def useful_function_b(self) -> None:
        pass

class ConcreteproductA1(AbstractProductA):
    def useful_function_a(self) -> str:
        return "the result of Product A1"

class ConcreteproductA2(AbstractProductA):
    def useful_function_a(self) -> str:
        return "the result of Product A2"

class ConcreteproductB1(AbstractProductB):
    def useful_function_b(self) -> str:
        return "the result of Product B1"

class ConcreteproductB2(AbstractProductB):
    def useful_function_b(self) -> str:
        return "the result of Product B2"

##### client code

def client_code(factory: AbstractFactory) -> None:
    product_a = factory.create_product_a()
    product_b = factory.create_product_b()

    print(f"{product_a.useful_function_a()}")
    print(f"{product_b.useful_function_b()}")


if __name__ == "__main__":
    print("client: Testing client code with the first factory type:")
    client_code(factory=ConcreteFactory1())

    print("\n")

    print("Client: Testing the same client code with the second factory type:")
    client_code(ConcreteFactory2())


