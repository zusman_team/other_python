import abc


############ Factories ############

class CarsFactory(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def createSedan(self):
        '''
        First way to define interface function that should be overrided
        :return:
        '''
        # return doesnt't match
        pass

    @abc.abstractmethod
    def createCoupe(self):
        '''
        Second way to define interface function that should be overrided
        :return:
        '''
        pass


class ToyotaFactory(CarsFactory):
    def createSedan(self):
        return ToyotaSedan()

    def createCoupe(self):
        return ToyotaCoupe()


class FordFactory(CarsFactory):
    def createSedan(self):
        return FordSedan()

    def createCoupe(self):
        return FordCoupe()


########### Products #############

class Sedan(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def makeShapeOfSedan(self) -> str:
        '''
        Third way to define interface function that should be overrided
        :return:
        '''
        raise NotImplementedError


class Coupe(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def makeShapeofCoupe(self) -> str:
        '''
        Third way to define interface function that should be overrided
        :return:
        '''
        raise NotImplementedError


class ToyotaSedan(Sedan):
    def makeShapeOfSedan(self) -> str:
        return "I made Toyota Sedan"


class FordSedan(Sedan):
    def makeShapeOfSedan(self) -> str:
        return "I made Ford Sedan"


class ToyotaCoupe(Coupe):
    def makeShapeofCoupe(self) -> str:
        return "I made Toyota Coupe"


class FordCoupe(Coupe):
    def makeShapeofCoupe(self) -> str:
        return "I made Ford Coupe"


##### client codeu


def client_code(factory: CarsFactory) -> None:
    product_S = factory.createSedan()
    product_C = factory.createCoupe()

    print(f"{product_C.makeShapeofCoupe()}")
    print(f"{product_S.makeShapeOfSedan()}")


if __name__ == "__main__":
    print("client: Testing client code with the first factory type:")
    client_code(factory=ToyotaFactory())

    # print("\n")

    print("Client: Testing the same client code with the second factory type:")
    client_code(factory=FordFactory())
