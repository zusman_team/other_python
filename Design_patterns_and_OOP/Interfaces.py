from typing import Protocol
import  abc
"""
this file shows several ways to build interface like in java, for example
"""

# fןrst way
class IInterface1(Protocol):
    @abc.abstractmethod
    def show(self):
        raise NotImplementedError

class MyClass1(IInterface1):
    def show(self):
        print("Hello World!")

class IInterface2(metaclass=abc.ABCMeta):
  @abc.abstractmethod
  def somemethod(self):
      return

class MyClass2(IInterface2):
    def show(self):
        print("Hello World2!")

    def somemethod(self):
        print("Something")


if __name__ == "__main__":
    x = MyClass1()
    x.show()
    y = MyClass2()
    y.somemethod()
    # y.show()