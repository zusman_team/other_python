import abc
import math

"""
It's not native to Python cause there is no interface, but it's demonstration  

"""
#enum of shapeName
class ShapeName(object):
    Retangle = "Retangle"
    Circle = "Circle"
    Triangle = "Triangle"


# building interface
class Shape(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def calculate_area(self):
        return

    @abc.abstractmethod
    def calculate_perimeter(self):
        return


# building concrete classes
class Retangle(Shape):
    def __init__(self, height, width):
        self.height = height
        self.width = width

    def calculate_area(self):
        return self.height * self.width

    def calculate_perimeter(self):
        return 2 * (self.height + self.width)


class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def calculate_area(self):
        return 3.14 * self.radius

    def calculate_perimeter(self):
        return 2 * 3.14 * self.radius


class Triangle(Shape):
    def __init__(self, a, b, c):
        if a+b<=c or a+c<b or b+c<a:
            raise ValueError("Each side should be less than sum of other one")
        self.a = a
        self.b = b
        self.c = c

    def calculate_area(self):
        """
        according Heron's formula
        :return:
        """
        p=self.calculate_perimeter()/2
        return math.sqrt(p*(p-self.a)*(p-self.b)*(p-self.c))

    def calculate_perimeter(self):
        return self.a +self.b + self.c

class ShapeFactory():
    def create_shape(self, name):
        if name == ShapeName.Retangle:
            height = input("Enter the height of the rectangle: ")
            width = input("Enter the width of the rectangle: ")
            return Retangle(int(height), int(width))
        if name == ShapeName.Circle:
            radius = input("Enter the radius of the circle: ")
            return Circle(int(radius))
        if name == ShapeName.Triangle:
            a = input("Enter first side of triangle: ")
            b = input("Enter second side of triangle: ")
            c = input("Enter third side of triangle: ")
            return Triangle(int(a), int(b), int(c))

def shapes_client():

    shape_factory = ShapeFactory()
    shape_name = input("Enter the name of the shape: ")

    shape = shape_factory.create_shape(shape_name)

    print(f"The type of object created: {type(shape)}")
    print(f"The area of the {shape_name} is: {shape.calculate_area()}")
    print(f"The perimeter of the {shape_name} is: {shape.calculate_perimeter()}")


if __name__ == '__main__':
    shapes_client()


"""
more native way
"""

