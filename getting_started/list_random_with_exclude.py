from random import randint, choice

listA = [x for x in range(10)]


def f1():
    y = choice(listA)
    listA.remove(y)
    print(y)


def f2():
    y = randint(0, len(listA) - 1)
    random_on_demand = listA[y]
    last_member = listA[-1]
    listA[-1] = random_on_demand
    listA[y] = last_member
    listA.pop()
    print(random_on_demand)
    return random_on_demand


def f3():
    global listA
    y = choice(listA)
    x1 = listA[0: listA.index(y)]
    x2 = listA[listA.index(y) + 1::]
    x1.extend(x2)
    listA = x1
    print(y, listA)


import time


def f4():
    global listA
    y = choice(listA)
    listA = [x for x in listA if x != y]
    print(id(listA))
    return y


if __name__ == "__main__":
    for g in range(5):
        f1()
