"""
This programm demonstartes Factory Design Pattern : all objects are in one file
add the purpose and usage --- TBD

"""

class Shape(object):
    # Create based on class name
    def factory(type):
        if type == "Circle": return Circle()
        if type == "Square": return Square()
        assert 0, "Bad Shape Creation: " + type

        staticmetod