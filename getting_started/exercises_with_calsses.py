class A:
    # Init.
    def __init__(self, value):
        self.__value = value

    # Two-underscore name.
    __value = 0

# Create the class.
a = A(5)

# [1] Cannot use two-underscore name.
# print(a.__value)

# [2] Must use mangled name.
print(a._A__value)

class Box:
    @staticmethod
    def Message(a):
        print("Box Message", a)

# Call static method with type.
Box.Message(1)