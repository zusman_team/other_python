import re
import sys
import os
import copy

dict1 = {"somekey1": "somevalueA1", "somekey2": "somevalueB1"}
dict2 = {"somekey1": "somevalueA1", "somekey2": "somevalueB2"}
dict3 = {"somekey1": "somevalueA3", "somekey2": "somevalueB3"}
dict4 = {"somekey1": "somevalueA4", "somekey2": "somevalueB4"}


# list1 = [dict1, dict2, dict3, dict4]


def fun1():
    list_for_comparasion = []
    list1 = \
        [{"somekey1": "somevalueA1", "somekey2": "somevalueB1"},
         {"somekey1": "somevalueA1", "somekey2": "somevalueB2"},
         {"somekey1": "somevalueA3", "somekey2": "somevalueB3"},
         {"somekey1": "somevalueA4", "somekey2": "somevalueB4"}]
    for dict in list1:
        list_for_comparasion = fun2(dict, list_for_comparasion)
    return list1


def fun2(some_dict, comp_list):
    if some_dict["somekey1"] not in comp_list:
        some_dict.update({"result": "OK"})
    else:
        some_dict.update({"result": "False"})
    comp_list.append(some_dict["somekey1"])
    return comp_list


def main():
    x = fun1()
    for i in x:
        print(i)


if __name__ == "__main__":
    main()
