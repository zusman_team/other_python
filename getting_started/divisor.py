"""
program that asks the user for a number and then prints out a list of all the divisors of that number.

"""

num = int(input("Enter the number for find its divisors"))
divisor = []
for elm in range(2,(int(num/2)+1)):
    if((num % elm) == 0):
        divisor.append(elm)

print(divisor)




