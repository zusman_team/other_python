"""
This program is for learning purpose

"""


def switch(x):
    return {
        "a": 25,
        "b": 46,
        "bhs": 33
    }.get(x, 22)


def main():
    print(switch("a"))
    print(switch("ffffffffffffffff"))
    print(switch("bhs"))


if __name__ == "__main__":
    main()
