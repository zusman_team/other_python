"""
This program splits input number (as integer) to digits and print sum of its digits method 1

"""

num = int(input("Enter number with 4 digits"))
sum = 0
print("The digits of a number are")
for i in range(4):
    num_int = int(num%10)
    print(num_int)
    sum += (num_int)
    num /= 10

print("The sum of digits is",sum)

