"""
This program splits input number (as integer) to digits and print sum of its digits method 2

"""

num = input("Enter number with 4 digits")
sum = 0
for digit in num:
    print(digit)
    sum += int(digit)

print ("The sum of digits is", sum)
print ("The sum is {0}".format(sum))