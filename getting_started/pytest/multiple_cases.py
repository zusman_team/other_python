
from unittest import TestCase

class TestClass(TestCase):
    def test_one(self):
        print("Test case1")
        x = "this"
        assert 'h' in x
    def test_two(self):
        print("Test case2")
        x = "hello"
        assert hasattr(x, 'check')

    def test_third(self):
        print("Test case3")
        x = "this"
        assert 'hy' in x

    def test_fourth(self):
        print("Test case4")
        x = "this"
        assert 'hi' in x
