# content of test_sample.py

from unittest import TestCase

class TestSolver1(TestCase):
    def func(x):
        return x + 1

    def test_answer():
        assert func(3) == 5