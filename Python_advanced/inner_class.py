class Human:
    def __init__(self):
        self.name = 'Guido'
        self.head = self.createHead()

    def createHead(self):
        return self.Head(self)

    class Head:
        def __init__(self, human):
            self.human = human

        def talk(self):
            return 'talking...', self.human.namein.think()


if __name__ == '__main__':
    guido = Human()
    guido.name
    guido.head.talk()

    guido.name = "Power Guido"
    guido.head.talk()
