# first-class objects
import functools


def say_hello(name):
    return f"Hello {name}"


def be_awesome(name):
    return f"Yo {name}, we are awesome"


def greet_bob(greater_function):
    return greater_function("Bob")


print(greet_bob(say_hello))

print(greet_bob(be_awesome))


# inner functions and returning functions from functions

def parent(num):
    def first_child():
        return "Hi, i am a first child"

    def second_child():
        return "Hi, i am a second child"

    if num == 1:
        return first_child
    else:
        return second_child


first = parent(1)
second = parent(2)

print(first())
print(second())


# decorator

def my_decorator(func):
    def wrapper():
        print("something happening before")
        func()
        print("something happening after")

    return wrapper


@my_decorator
def say_whee():
    print("wee")


# say_whee = my_decorator(say_whee)

say_whee()


def do_twice(func):
    def wrapper_do_twice(*args, **kwargs):
        for i in range(3):
            func(*args, **kwargs)

    return wrapper_do_twice


@do_twice
def greet(name):
    print(f"Hello {name}")


greet("Pablo")


def repeat(num_times):
    def decorator_repeat(func):
        @functools.wraps(func)
        def wrapper_repeat(*args, **kwargs):
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value

        return wrapper_repeat

    return decorator_repeat


@repeat(num_times=10)
def greet1(name):
    print(f"Hello {name}")


greet1("Rudy")


def do_triple(func):
    def wrapper_do_twice(*args, **kwargs):
        func(*args, **kwargs)
        if "Tal" in args:
            return func(*args, **kwargs), ["sso", "ss1"]
        elif "Oren" in args:
            return func(*args, **kwargs), ["sso", "ss1"]
        else:
           return func(*args, **kwargs)
        # return f"{func(*args, **kwargs)}- {stages}"

        # return stages

    return wrapper_do_twice


@do_triple
def greet3(name):
    print(f"Hello {name}")
    return name


x,y = greet3("Tal")
x1 = greet3("bla")
# print(greet3("Tal"))
print("\n")
# print(greet3("Oren"))
