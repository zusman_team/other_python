import time

import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello')

bbb = {}


def callback(ch, method, properties, body):
    # while True:
    #     for method, properties, body in channel.consume('hello', arguments=bbb, inactivity_timeout=5):
    #         print(" [x] Received %r" % body)
    #         # channel.basic_ack(method.delivery_tag)
    #     print("got inactivity timeout")
    print(" [x] Received %r" % body)


channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback,
                      arguments= bbb)

# print(' [*] Waiting for messages. To exit press CTRL+C')
# channel.basic_get(queue='hello')
channel.start_consuming()
time.sleep(10)
channel.stop_consuming()
for x, y in bbb.items():
    print(x, y)
