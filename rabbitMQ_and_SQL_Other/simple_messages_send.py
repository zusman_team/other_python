import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello')

#channel.exchange_declare(exchange='logs', exchange_type='fanout')

for i in range(5):
    channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=f'Hello World_{i}')
    print(" [x] Sent 'Hello World!'")
#connection.close()

