import re
from anytree import Node, RenderTree
from anytree.search import find, find_by_attr, findall_by_attr, findall

udo = Node("Udo")
marc = Node("Marc", parent=udo)
lian = Node("Lian", parent=marc)
dan = Node("Dan", parent=udo)
jet = Node("Jet", parent=dan)
jan = Node("Jan", parent=dan)
joe = Node("Joe", parent=dan)

print(udo)
Node('/Udo')
print(joe)
Node('/Udo/Dan/Joe')

for pre, fill, node in RenderTree(udo):
    print("%s%s" % (pre, node.name))
    # print("%s%s" % (pre, node))
    # print("%s" % (pre))

def get_ancestors_for_level(some_tree_obj, level=0):
    pass
    # if level
print("")
print(RenderTree(udo))
print("")

node1 = find(udo, lambda node: bool(re.search(".*Da.*", node.name)), maxlevel=3)
node2 = findall(udo, lambda node: node.depth == 1)
# findall_by_attr()

# for pre, fill, node in RenderTree(udo):
#     if node == dan:
#         print([i.name for i in node.children])


print(find(node = find(udo, lambda node: node.name == "Jet")))
print(find_by_attr(udo, "Jet"))
print("")
print(find_by_attr(udo, "Jet", maxlevel=1))

jan.ancestors[1]


op = Node("op")
marc = Node("Marc", parent=udo)
lian = Node("Lian", parent=marc)
dan = Node("Dan", parent=udo)
jet = Node("Jet", parent=dan)
jan = Node("Jan", parent=dan)
joe = Node("Joe", parent=dan)