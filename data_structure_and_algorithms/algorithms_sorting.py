"""
This programs implements different types of sorting algorithms
"""
import inspect
import re
#change

# input list of unsorted elements (integers)

number_list = [int(x) for x in input("Enter list of integers with space between them").split()] \
              or [3, 1, 9, 5, 6, 4, 8]

print("Choose sorting algorithms\n")
print("0 - for Bubble sort\n")
print("1 - for Insertion sort\n")
print("2 - for Selection sort\n")
# sorting_alg = int(input("Enter your choice\n"))

complexity = 0

# range in python3 like a xrange in python2 created value on demand reducing memory capacity
# Bubble sorting - Complexity O(n^2)
h = 0  # external for iterator for complexity calculatuing
y = 0  # inner for iterator for complexity calculating


# method 1 - start from the left (biggest is sinking/floating)

# general for all languages
def bubble_sort_general_method(a_list):
    m = len(a_list)
    for i in range(0, m - 1):
        # print(" i = ", i)
        for j in range(0, m - i - 1):
            if a_list[j] > a_list[j + 1]:
                misc_swap = a_list[j + 1]
                a_list[j + 1] = a_list[j]
                a_list[j] = misc_swap
                #   print(" j = ", j)

def bubble_sort_python_method1(a_list):
    m = len(a_list)
    for i in range(0, m - 1):
        # print(" i = ", i)
        for j in range(0, m - i - 1):
            if a_list[j] > a_list[j + 1]:
                a_list[j], a_list[j + 1] = a_list[j + 1], a_list[j]

# python manner:
def bubble_sort_python_method2(a_list):
    m = len(a_list)
    sorted = False
    while not sorted:
        sorted = True
        for position in range(m - 1):
            if a_list[position] > a_list[position + 1]:
                # swap
                a_list[position], a_list[position + 1] = a_list[position + 1], a_list[position]
                sorted = False

def bubble_sort_python_method3(a_list):
    m = len(a_list)
    sorted = True
    while sorted:
        sorted = False
        for position in range(m - 1):
            if a_list[position] > a_list[position + 1]:
                # swap
                a_list[position], a_list[position + 1] = a_list[position + 1], a_list[position]
                sorted = True


def bubble_sort_pyhon_method4(a_list):
    passnum = len(a_list) - 1
    exchange = True
    while passnum > 0 and exchange:
        exchange = False
        for i in range(passnum):
            if a_list[i] > a_list[i + 1]:
                # swap
                exchange = True
                misc = a_list[i]
                a_list[i] = a_list[i + 1]
                a_list[i + 1] = misc
        passnum -= 1


# Insert Sorting

# with additional array
def insert_sorting_method1(a_list):
    m = len(a_list)
    a_list_sorted = [a_list[0]]
    for i in range[1, m - 1]:
        a_list_sorted.append(a_list[i])
        for j in range(i, 0, -1):
            if a_list_sorted[j] < a_list_sorted[j - 1]:
                # swap
                a_list_sorted[j], a_list_sorted[j - 1] = a_list[j - 1], a_list_sorted[j - 1]


# without additional array
def insert_sorting_method2(a_list):
    for i in range(1, len(a_list)):
        j = i
        while j > 0 and a_list[j] < a_list[j - 1]:
            a_list[j], a_list[j - 1] = a_list[j - 1], a_list[j]
            j -= 1


def insert_sorting_method3(a_list):
    print("Insertion sort - method3")
    for i in range(1, len(a_list)):
        val_to_insert = a_list[i]
        hole_to_insert = i
        while hole_to_insert > 0 and a_list[hole_to_insert - 1] > val_to_insert:
            a_list[hole_to_insert] = a_list[hole_to_insert - 1]
            hole_to_insert -= 1
        a_list[hole_to_insert] = val_to_insert


# Selection Sorting

def selection_sort_method1(a_list):
    print("Selection sort - method1")
    for i in range(0, len(a_list) - 1):
        min_location = i
        for j in range((i + 1), len(a_list)):
            if a_list[j] < a_list[min_location]:
                min_location = j
        #swap with menebr in found min_location
        temp = a_list[i]
        a_list[i] = a_list[min_location]
        a_list[min_location] = temp

def selection_sort_method2(a_list):
    print("Selection sort - method1")
    for i in range(0, len(a_list) - 1):
        min_location = i
        for j in range((i + 1), len(a_list)):
            if a_list[j] < a_list[min_location]:
                min_location = j
        # swap with member in found min_location
        a_list[i],a_list[min_location] = a_list[min_location],a_list[i]

def selection_sort_method3(a_list):
    print("Selection sort - method1")
    for i in range(0, len(a_list) - 1):
        min_location = i
        for j in range((i + 1), len(a_list)):
            if a_list[j] < a_list[min_location]:
                min_location = j
        # swap with menebr in found min_location
        a_list[i], a_list[min_location] = a_list[min_location], a_list[i]




# merge sorting

def merge_sort_method(a_list):
    pass


# bubble_sort_python_method(number_list)
# insert_sorting_method3(number_list)
#selection_sort_method3(number_list)



if __name__ == '__main__':
    alg_s = {}
    index = 0
    for i in dir():
        if re.search(".*sort*", i):
            alg_s[str(index)] = i
            print(f"{index} - {i}")
            index += 1
    alg_num=input("Enter your choice\n")
    alf_req= alg_s[alg_num]
    func = eval(alf_req)
    func(number_list)
    print("List you wanted is as following ", number_list)

