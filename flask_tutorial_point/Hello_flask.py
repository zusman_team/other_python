from flask import Flask, redirect, url_for, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    '''
    Basic route
    :return:
    '''
    return 'Hello World_123555w21212ww'

# other represantion of routes
def hello_world13():
    '''
    Basic route
    :return:
    '''
    return 'Hello World_13'

app.add_url_rule('/', 'other', hello_world)


@app.route('/hello_world')
def hello_world1():
    '''
    Routing
    :return:
    '''
    return 'Hello World1_133'


@app.route('/hello/<name>')
def hello_name(name):
    '''
    bla-bla -- " \''' and enter
    Varaible routes
    :return:
    '''
    return 'Hello %s!' % name


@app.route('/blog/<int:postID>')
def show_blog(postID):
    return 'Blog Number %d' % postID


@app.route('/rev/<float:revNo>')
def revision(revNo):
    return 'Revision Number %f' % revNo


@app.route('/ppp/')
def canonical():
    '''
    Caninical with trailing '/'
    :param revNo:
    :return:
    '''
    return 'Hello Python'


'''
dynamic building of URL

'''


@app.route('/admin')
def hello_admin():
    return 'Hello Admin'


@app.route('/guest/<guest>')
def hello_guest(guest):
    return 'Hello %s as Guest' % guest


@app.route('/user/<name1>')
def hello_user(name1):
    if name1 == 'admin':
        return redirect(url_for('hello_admin'))
    else:
        return redirect(url_for('hello_guest', guest=name1))

'''
HTTP methods
'''
@app.route('/success/<name>')
def success(name):
   return "welcome %s" % name


@app.route('/login',methods = ['POST', 'GET'])
def login():
   if request.method == 'POST':
      user = request.form['nm']
      return redirect(url_for('success',name = user))
   else:
      user = request.args.get('nm')
      return redirect(url_for('success',name = user))

if __name__ == '__main__':
    app.debug = True
    # app.run()
    app.run(debug=True)
    # app.run() # default running with debug false and port 5000
