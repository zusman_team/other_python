from flask import Flask, redirect, url_for, request, render_template
'''
here excercises and examples for tutorialpoint Flask
'''
app = Flask(__name__)

# hardcoded template
@app.route('/111')
def index():
    '''
    Basic route
    :return:
    '''

    return '<html><body><h1>Hello World</h1></body></html>'

# dynamic template
from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
   return render_template("hello.html")


@app.route('/hello/<user>')
def hello_name(user):
   return render_template('hello.html', name = user)

@app.route('/hello/score/<int:score>')
def hello_name2(score):
   return render_template('hello2.html', marks = score)

@app.route('/result')
def result():
   dict = {'phy':50,'che':60,'maths':70}
   return render_template('result.html', result = dict)

# if __name__ == '__main__':
#    app.run(debug = True)


if __name__ == '__main__':
    app.debug = True
    # app.run()
    app.run(debug=True)
    # app.run() # default running with debug false and port 5000
